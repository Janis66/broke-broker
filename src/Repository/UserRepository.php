<?php


namespace App\Repository;

use App\Database\ConnectionHandler;

class UserRepository extends Repository
{
    protected $tableName = 'User';

    public function getBalance($email)
    {
        $query = "SELECT * FROM `user` WHERE email=?";

        $statement = ConnectionHandler::getConnection()->prepare($query);
        $statement->bind_param('s', $email);

        $statement->execute();

        $result = $statement->get_result();
        $row = $result->fetch_assoc();


        return $row['kapital'];
    }

    public function getTimestamp($email)
    {
        $query = "SELECT * FROM `user` WHERE email=?";

        $statement = ConnectionHandler::getConnection()->prepare($query);
        $statement->bind_param('s', $email);

        $statement->execute();

        $result = $statement->get_result();
        $row = $result->fetch_assoc();


        return $row['time_stamp'];
    }

    public function getfirstName($email)
    {
        $query = "SELECT * FROM `user` WHERE email=?";

        $statement = ConnectionHandler::getConnection()->prepare($query);
        $statement->bind_param('s', $email);

        $statement->execute();

        $result = $statement->get_result();
        $row = $result->fetch_assoc();


        return $row['firstName'];
    }

    public function getlastName($email)
    {
        $query = "SELECT * FROM `user` WHERE email=?";

        $statement = ConnectionHandler::getConnection()->prepare($query);
        $statement->bind_param('s', $email);

        $statement->execute();

        $result = $statement->get_result();
        $row = $result->fetch_assoc();


        return $row['lastName'];
    }

    public function getPassword($email)
    {
        $query = "SELECT * FROM `user` WHERE email=?";

        $statement = ConnectionHandler::getConnection()->prepare($query);
        $statement->bind_param('s', $email);

        $statement->execute();

        $result = $statement->get_result();
        $row = $result->fetch_assoc();


        $password = $row["password"];
        return $password;
    }

    public function setBalance($email, $balance)
    {
        $query = "UPDATE `user` SET kapital = ? WHERE email=?";

        $statement = ConnectionHandler::getConnection()->prepare($query);
        $statement->bind_param('is', $balance, $email);

        $statement->execute();
    }

    public function setStock($email, $stock, $amount)
    {
        if ($stock == "LISN.SW") {
            $stock = "LISN_SW";
        }
        $stockName = $stock . "_Amount";
        $query = "UPDATE `user` SET $stockName = ? WHERE email=?";
        $statement = ConnectionHandler::getConnection()->prepare($query);
        $statement->bind_param('is', $amount, $email);

        $statement->execute();
    }

    public function getStockAmount($stock, $email)
    {
        if ($stock == "LISN.SW") {
            $stock = "LISN_SW";
        }

        $stockName = $stock . "_Amount";
        $query = "SELECT * FROM `user` WHERE email=?";

        $statement = ConnectionHandler::getConnection()->prepare($query);
        $statement->bind_param('s', $email);

        $statement->execute();

        $result = $statement->get_result();
        $row = $result->fetch_assoc();


        return $row[$stockName];
    }

    public function exists($email, $password)
    {
        $query = "SELECT * FROM {$this->tableName} WHERE email=? and password=?";

        $statement = ConnectionHandler::getConnection()->prepare($query);


        $password = hash('sha256', $password);
        $statement->bind_param('ss', $email, $password);

        $statement->execute();


        $result = $statement->get_result();
        if (!$result) {
            throw new Exception($statement->error);
        }
        return $result->num_rows == 1;
    }

    public function existsEmail($email)
    {
        $query = "SELECT * FROM {$this->tableName} WHERE email=?";

        $statement = ConnectionHandler::getConnection()->prepare($query);


        $statement->bind_param('s', $email);

        $statement->execute();


        $result = $statement->get_result();
        if (!$result) {
            throw new Exception($statement->error);
        }
        return $result->num_rows == 1;
    }

    public function setPassword($password)
    {

        $query = "UPDATE `user` SET password=? WHERE email=?";

        $statement = ConnectionHandler::getConnection()->prepare($query);
        $password = hash('sha256', $password);
        $statement->bind_param('ss', $password, $_SESSION["usedEmail"]);

        $statement->execute();
    }


    public function create($firstName, $lastName, $email, $password)
    {
        // Query erstellen
        $query = "INSERT INTO `user` (firstName, lastName, email, password, DELL_Amount, GOOGL_Amount, LISN_SW_Amount, GOLD_Amount, AAPL_Amount, USO_Amount, NVDA_Amount, BTC_Amount, UBS_Amount, CS_Amount, INTC_Amount, AMD_Amount) VALUES (?, ?, ?, ?, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);";

        // Datenbankverbindung anfordern und, das Query "preparen" (vorbereiten)
        // und die Parameter "binden"
        $statement = ConnectionHandler::getConnection()->prepare($query);
        $password = hash('sha256', $password);
        $statement->bind_param('ssss', $firstName, $lastName, $email, $password);

        // Das Statement absetzen
        $statement->execute();
    }
}