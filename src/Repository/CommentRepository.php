<?php


namespace App\Repository;

use App\Database\ConnectionHandler;

class CommentRepository extends Repository
{
    protected $tableName = 'Comment';

    public function create($name, $text)
    {
        // Query erstellen
        $query = "INSERT INTO `comment` (name, text) VALUES (?, ?);";

        // Datenbankverbindung anfordern und, das Query "preparen" (vorbereiten)
        // und die Parameter "binden"
        $statement = ConnectionHandler::getConnection()->prepare($query);

        $statement->bind_param('ss', $name, $text);

        // Das Statement absetzen
        $statement->execute();
    }

    public function readAll($max = 100)
    {
        $query = "SELECT * FROM `comment` LIMIT 0, $max";

        $statement = ConnectionHandler::getConnection()->prepare($query);
        $statement->execute();

        $result = $statement->get_result();
        if (!$result) {
            throw new Exception($statement->error);
        }

        // Datensätze aus dem Resultat holen und in das Array $rows speichern
        $rows = array();
        while ($row = $result->fetch_object()) {
            $rows[] = $row;
        }

        return $rows;
    }

    public function getName($id)
    {
        $query = "SELECT * FROM `comment` WHERE id=?";

        $statement = ConnectionHandler::getConnection()->prepare($query);
        $statement->bind_param('i', $id);

        $statement->execute();

        $result = $statement->get_result();
        $row = $result->fetch_assoc();


        return $row['name'];
    }

    public function getText($id)
    {
        $query = "SELECT * FROM `comment` WHERE id=?";

        $statement = ConnectionHandler::getConnection()->prepare($query);
        $statement->bind_param('i', $id);

        $statement->execute();

        $result = $statement->get_result();
        $row = $result->fetch_assoc();


        return $row['text'];
    }
}