<?php

namespace App\Controller;

use App\Repository\CommentRepository;
use App\Repository\UserRepository;
use App\View\View;

/**
 * Der Controller ist der Ort an dem es für jede Seite, welche der Benutzer
 * anfordern kann eine Methode gibt, welche die dazugehörende Businesslogik
 * beherbergt.
 *
 * Welche Controller und Funktionen muss ich erstellen?
 *   Es macht sinn, zusammengehörende Funktionen (z.B: User anzeigen, erstellen,
 *   bearbeiten & löschen) gemeinsam in einem passend benannten Controller (z.B:
 *   UserController) zu implementieren. Nicht zusammengehörende Features sollten
 *   jeweils auf unterschiedliche Controller aufgeteilt werden.
 *
 * Was passiert in einer Controllerfunktion?
 *   Die Anforderungen an die einzelnen Funktionen sind sehr unterschiedlich.
 *   Folgend die gängigsten:
 *     - Dafür sorgen, dass dem Benutzer eine View (HTML, CSS & JavaScript)
 *         gesendet wird.
 *     - Daten von einem Model (Verbindungsstück zur Datenbank) anfordern und
 *         der View übergeben, damit diese Daten dann für den Benutzer in HTML
 *         Code umgewandelt werden können.
 *     - Daten welche z.B. von einem Formular kommen validieren und dem Model
 *         übergeben, damit sie in der Datenbank persistiert werden können.
 */
class DefaultController
{
    /**
     * Die index Funktion des DefaultControllers sollte in jedem Projekt
     * existieren, da diese ausgeführt wird, falls die URI des Requests leer
     * ist. (z.B. http://my-project.local/). Weshalb das so ist, ist und wann
     * welcher Controller und welche Methode aufgerufen wird, ist im Dispatcher
     * beschrieben.
     */
    public function index()
    {
        // In diesem Fall möchten wir dem Benutzer die View mit dem Namen
        //   "default_index" rendern. Wie das genau funktioniert, ist in der
        //   View Klasse beschrieben.
        $view = new View('default/index');
        $view->title = 'Homepage';
        $view->heading = '';
        $view->display();
    }

    public function account()
    {

        $userRepository = new UserRepository();
        $view = new View('default/account');
        $view->title = 'Account';
        $view->heading = '';
        $view->firstName = $userRepository->getfirstName($_SESSION["usedEmail"]);
        $view->lastName = $userRepository->getlastName($_SESSION["usedEmail"]);
        $view->timeStamp = $userRepository->getTimestamp($_SESSION["usedEmail"]);
        $view->display();
    }

    public function transfer()
    {
        $view = new View('default/transfer');
        $view->title = 'Transfer';
        $view->heading = 'Transfer';
        $view->alertMessage = '';

        if (!isset($_SESSION["transferToOwnAccount"])) {
            $_SESSION["transferToOwnAccount"] = false;
        }

        if (!isset($_SESSION["userDoesntExist"])) {
            $_SESSION["userDoesntExist"] = false;
        }

        if (!isset($_SESSION["notEnoughMoney"])) {
            $_SESSION["notEnoughMoney"] = false;
        }

        if (!isset($_SESSION["lessThanOne"])) {
            $_SESSION["lessThanOne"] = false;
        }

        if ($_SESSION["transferToOwnAccount"]) {
            $view->alertMessage = 'You cant transfer money to your own account!';
            $_SESSION["transferToOwnAccount"] = false;
        }

        if ($_SESSION["userDoesntExist"]) {
            $view->alertMessage = 'There exists no user with that email!';
            $_SESSION["userDoesntExist"] = false;
        }

        if ($_SESSION["lessThanOne"]) {
            $view->alertMessage = 'You cant transfer less than 1$!';
            $_SESSION["lessThanOne"] = false;
        }

        if ($_SESSION["notEnoughMoney"]) {
            $view->alertMessage = 'You dont have enough money in your account to complete this transfer!';
            $_SESSION["notEnoughMoney"] = false;
        }

        if (!isset($_SESSION["loggedIn"]) || $_SESSION["loggedIn"] == false) {
            $view->alertMessage = 'You have to be logged in to transfer money!';
        }

        $view->display();
    }


    public function doTransfer()
    {
        if (isset($_POST['send']) && $_POST['send'] != "" ) {
            if ($_SESSION['loggedIn']) {
                if (isset($_POST['email']) && isset($_POST['amount'])) {
                    $transferringEmail = htmlspecialchars($_POST['email']);
                    $amount = htmlspecialchars($_POST['amount']);
                    $userrepository = new UserRepository();

                    if ($userrepository->existsEmail($transferringEmail) == false) {
                        $_SESSION['userDoesntExist'] = true;
                    }

                    if ($userrepository->getBalance($_SESSION["usedEmail"]) >= $amount) {
                        if ($transferringEmail != $_SESSION["usedEmail"]) {
                            $userrepository->setBalance($transferringEmail, $userrepository->getBalance($transferringEmail) + $amount);
                            $userrepository->setBalance($_SESSION["usedEmail"], $userrepository->getBalance($_SESSION["usedEmail"]) - $amount);
                        } else {
                            $_SESSION["transferToOwnAccount"] = true;
                        }
                    } else {
                        $_SESSION["notEnoughMoney"] = true;
                    }
                }
            }
        }
        header('Location: /default/transfer');
        return;
    }

    public
    function leaderboard()
    {

        $userRepository = new UserRepository();
        $view = new View('default/leaderboard');
        $view->title = 'Leaderboard';
        $view->heading = 'Leaderboard';
        $view->users = $userRepository->readCurrentLeaderboardUsers();
        $view->users3 = $userRepository->readCurrentTop3();
        $view->display();
    }

    public function comment()
    {
        $commentRepository = new CommentRepository();
        $view = new View('default/comment');
        $view->title = 'comment';
        $view->heading = '';
        $view->commentID = '';
        $view->commentName = '';
        $view->commentText = '';
        $_SESSION["edit"] = false;
        $view->comments = $commentRepository->readAll();
        $view->display();
    }

    public function doComment()
    {

        $_SESSION["edit"] = false;

        $commentRepository = new CommentRepository();
        $name = htmlspecialchars($_POST['name']);
        $text = htmlspecialchars($_POST['text']);

        $commentRepository->create($name, $text);


        header('Location: /default/comment');
        return;
    }

    public
    function stocks()
    {

        $view = new View('default/stocks');
        $view->title = 'Stocks';
        $view->heading = 'Stocks';
        $view->alertMessage = '';

        if (!isset($_SESSION["stockNotEnoughMoney"])) {
            $_SESSION["stockNotEnoughMoney"] = false;
        }

        if (!isset($_SESSION["amountUnder0"])) {
            $_SESSION["amountUnder0"] = false;
        }

        if (!isset($_SESSION["stockNoAmount"])) {
            $_SESSION["stockNoAmount"] = false;
        }

        if ($_SESSION["stockNotEnoughMoney"] == true) {
            $view->alertMessage = 'You dont have enough money!';
            $_SESSION["stockNotEnoughMoney"] == false;
        }

        if ($_SESSION["stockNoAmount"] == true) {
            $view->alertMessage = 'You cant sell more than you have bought of this stock!';
            $_SESSION["stockNoAmount"] == false;
        }

        if ($_SESSION["amountUnder0"] == true) {
            $view->alertMessage = 'The Amount has to be over 0!';
            $_SESSION["amountUnder0"] == false;
        }

        if (!isset($_SESSION["loggedIn"]) || $_SESSION["loggedIn"] == false) {
            $view->alertMessage = 'You have to be logged in to buy or sell stocks!';
        }

        $view->userrepository = new UserRepository();

        $json = file_get_contents('http://share.g-7.ch/latest');

        $view->data = json_decode($json);

        $view->display();
    }

    function buyStock()
    {
        if (isset($_POST['send'])) {
            $stockName = htmlspecialchars($_POST['stockName']);
            $amount = htmlspecialchars($_POST['amount']);
            $userrepository = new UserRepository();

            $json = file_get_contents('http://share.g-7.ch/latest');
            $stocks = json_decode($json);

            foreach ($stocks as $stock):
                if ($stock->name == $stockName) {
                    $value = $stock->value;
                }
            endforeach;
            $price = $amount * $value;
            if ($amount > 0) {
                if ($userrepository->getBalance($_SESSION["usedEmail"]) >= $price) {
                    $userrepository->setBalance($_SESSION["usedEmail"], $userrepository->getBalance($_SESSION["usedEmail"]) - $price);
                    $newAmount = $userrepository->getStockAmount($stockName, $_SESSION["usedEmail"]) + $amount;
                    $userrepository->setStock($_SESSION["usedEmail"], $stockName, $newAmount);
                }
            }
        }

        header('Location: /default/stocks');
        return;
    }

    function sellStock()
    {
        if (isset($_POST['send'])) {
            $stockName = htmlspecialchars($_POST['stockName']);
            $amount = htmlspecialchars($_POST['amount']);
            $userrepository = new UserRepository();

            $json = file_get_contents('http://share.g-7.ch/latest');
            $stocks = json_decode($json);
            if ($amount > 0) {
                foreach ($stocks as $stock):
                    if ($stock->name == $stockName) {
                        $value = $stock->value;
                    }
                endforeach;
                if ($userrepository->getStockAmount($stockName, $_SESSION["usedEmail"]) > 0) {
                    if ($userrepository->getStockAmount($stockName, $_SESSION["usedEmail"]) >= $amount) {
                        $price = $amount * $value;
                        $userrepository->setBalance($_SESSION["usedEmail"], $userrepository->getBalance($_SESSION["usedEmail"]) + $price);
                        $newAmount = $userrepository->getStockAmount($stockName, $_SESSION["usedEmail"]) - $amount;
                        $userrepository->setStock($_SESSION["usedEmail"], $stockName, $newAmount);
                    } else {
                        $_SESSION["stockNoAmount"] = true;
                    }
                }
            } else {
                $_SESSION["amountUnder0"] = true;
            }

        }

        header('Location: /default/stocks');
        return;
    }

    public function delete()
    {
        $commentRepository = new commentRepository();
        $commentRepository->deleteById($_GET['id']);

        // Anfrage an die URI /user weiterleiten (HTTP 302)
        header('Location: /default/comment');
    }

    public function edit()
    {
        $view = new View('/default/comment');
        $view->title = 'Comment';
        $view->heading = '';

        $commentRepository = new commentRepository();
        $id = $_GET['id'];


        $view->commmentID = $id;
        $_SESSION["edit"] = true;
        $view->commentName = $commentRepository->getName($id);
        $view->commentText = $commentRepository->getText($id);

        $commentRepository->deleteById($id);


        $view->display();
    }
}
