<?php

namespace App\Controller;

use App\Repository\UserRepository;
use App\View\View;

/**
 * Siehe Dokumentation im DefaultController.
 */
class UserController
{
    public function index()
    {
        $userRepository = new UserRepository();

        $view = new View('user/index');
        $view->title = 'User';
        $view->heading = 'User';
        $view->users = $userRepository->readAll();
        $view->display();
    }


    public function login()
    {
        $view = new View('user/login');
        $view->title = 'Login';
        $view->heading = 'Login';
        $view->alertMessage = '';

        if (!isset($_SESSION["wrongCredentials"])) {
            $_SESSION["wrongCredentials"] = false;
        }

        if ($_SESSION["wrongCredentials"]) {
            $view->alertMessage = 'The email or password you have entered is invalid!';
            $_SESSION["wrongCredentials"] = false;
        }

        $view->display();
    }

    public function doLogin()
    {
        if (isset($_POST['send'])) {
            $email = htmlspecialchars($_POST['email']);
            $password = htmlspecialchars($_POST['password']);

            $userRepository = new UserRepository();
            if ($userRepository->exists($email, $password)) {
                $_SESSION["loggedIn"] = true;
                $_SESSION["usedEmail"] = htmlspecialchars($_POST["email"]);
                header('Location: /');
                return;
            } else {
                $_SESSION["wrongCredentials"] = true;
            }

        }
        header('Location: /user/login');
        return;
        // Anfrage an die URI /user weiterleiten (HTTP 302)

    }

    public function changePassword()
    {
        $view = new View('user/changePassword');
        $view->title = 'changePassword';
        $view->heading = 'Change you Password';
        $view->display();
    }

    public function doChangePassword()
    {
        if (isset($_POST['send'])) {
            $password = htmlspecialchars($_POST['password']);
            $passwordR = htmlspecialchars($_POST['password-repeat']);
            $passwordO = htmlspecialchars($_POST['password-old']);

            $userRepository = new UserRepository();
            $passwordO = hash('sha256', $passwordO);
            $passwordDB = $userRepository->getPassword($_SESSION["usedEmail"]);
            if ($passwordO == $passwordDB) {
                if ($password == $passwordR) {
                    $userRepository->setPassword($password);
                } else {
                    header('Location: /user/changePassword?error=passwords_do_not_match');
                    exit;
                }
            } else {
                header('Location: /user/changePassword?error=Current_password_is_not_valid');
                exit;
            }
            header('Location: /default/account');
            exit;
        }

        // Anfrage an die URI /user weiterleiten (HTTP 302)
        header('Location: /user/changePassword');
        return;
    }

    public function create()
    {
        $view = new View('user/create');
        $view->title = 'Register';
        $view->heading = 'Register';
        $view->alertMessage = '';

        if (!isset($_SESSION["sameEmailTwice"])) {
            $_SESSION["sameEmailTwice"] = false;
        }

        if ($_SESSION["sameEmailTwice"]) {
            $view->alertMessage = 'This email has already been used!';
            $_SESSION["sameEmailTwice"] = false;
        }

        $view->display();
    }

    public function doCreate()
    {
        if (isset($_POST['send'])) {
            $firstName = htmlspecialchars($_POST['fname']);
            $lastName = htmlspecialchars($_POST['lname']);
            $email = htmlspecialchars($_POST['email']);
            $password = htmlspecialchars($_POST['password']);

            $userRepository = new UserRepository();
            $users = $userRepository->readAll();
            foreach ($users as $user):
                if ($user->email == $email) {
                    $_SESSION["sameEmailTwice"] = true;
                    header('Location: /user/create');
                    return;
                }
            endforeach;
            $userRepository->create($firstName, $lastName, $email, $password);
            $userRepository->setBalance($email, 1000);
            header('Location: /user/login');
            return;
        }

        // Anfrage an die URI /user weiterleiten (HTTP 302)
        header('Location: /user/create');
        return;
    }

    public function delete()
    {
        $userRepository = new UserRepository();
        $userRepository->deleteById($_GET['id']);

        // Anfrage an die URI /user weiterleiten (HTTP 302)
        header('Location: /user');
    }

    public function logout()
    {
        session_start();
        $_SESSION["loggedIn"] = false;
        header('Location: /');
    }
}
