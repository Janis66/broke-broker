<h2 id="alertMessage" ><?= $alertMessage ?></h2>
<div class="row">
    <div id="fixFooter"class="col-3">
        <?php foreach ($data as $stock): ?>
            <p class="stockAndValue">Stock: <?= $stock->name; ?> / Current Value: <?= $stock->value; ?></p>
            <?php if ($_SESSION["loggedIn"]) { ?>
                <div id="yourAmount">Your Amount: <?= $userrepository->getStockAmount($stock->name, $_SESSION["usedEmail"]) ?></div>
            <?php } ?>
        <?php endforeach; ?>
    </div>
    <div id="StockMid"class="col-6">
        <h1>Buy Stocks</h1>
        <form action="/default/buyStock" method="post">
            <div class="form-group">
                <label for="stockName">Stock name</label>
                <select name="stockName" id="stockName">
                    <?php foreach ($data as $stock): ?>
                        <option value="<?= $stock->name; ?>"><?= $stock->name; ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="form-group">
                <label for="amount">Amount</label>
                <input id="amount" name="amount" type="number" class="form-control">
            </div>
            <button id="bottomMargin" type="submit" name="send" class="btn btn-primary">Submit</button>
        </form>
        <h1>Sell Stocks</h1>
        <form action="/default/sellStock" method="post">
            <div class="form-group">
                <label for="stockName">Stock name</label>
                <select name="stockName" id="stockName">
                    <?php foreach ($data as $stock): ?>
                        <option value="<?= $stock->name; ?>"><?= $stock->name; ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="form-group">
                <label for="amount">Amount</label>
                <input id="amount" name="amount" type="number" class="form-control">
            </div>
            <button id="bottomMargin" type="submit" name="send" class="btn btn-primary">Submit</button>
        </form>
    </div>
    <div class="col-3"></div>
</div>