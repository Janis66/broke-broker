<article class="hreview open special">
    <div class="row">
        <div class="col-6">
            <?php if (empty($users)): ?>
                <div class="dhd">
                    <h2 class="item title">No users found.</h2>
                </div>
            <?php else: ?>
            <?php $rank = 1;
            foreach ($users as $user): ?>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <p class="description"><?= $rank; ?>. <?= $user->firstName; ?> <?= $user->lastName; ?>:
                            <a><?= $user->kapital; ?>.00 $</a></p>
                    </div>
                    <?php $rank++; ?>
                </div>
            <?php endforeach; ?>
        </div>
        <div class="col-6">
            <?php $userOrder = 1;
            foreach ($users3 as $user): ?>
                <?php if ($userOrder == 1) { ?>
                    <div>
                        <p class="top3"><?= $user->firstName; ?> <?= $user->lastName; ?>:
                            <a><?= $user->kapital; ?>.00 $</a></p>
                    </div>
                    <img class="top3Pic" src="/images/First place.png" style="width:20%">
                <?php } elseif ($userOrder == 2) { ?>
                    <div>
                        <p class="top3"><?= $user->firstName; ?> <?= $user->lastName; ?>:
                            <a><?= $user->kapital; ?>.00 $</a></p>
                    </div>
                    <img class="top3Pic" src="/images/Secound place.png" style="width:20%">
                <?php } else { ?>
                    <div>
                        <p class="top3"><?= $user->firstName; ?> <?= $user->lastName; ?>:
                            <a><?= $user->kapital; ?>.00 $</a></p>
                    </div>
                    <img class="top3Pic" src="/images/Third place.png" style="width:20%">
                <?php }
                $userOrder = $userOrder + 1; ?>
            <?php endforeach; ?>
            <?php endif; ?>
        </div>
    </div>
</article>
