<h2 id="alertMessage" ><?= $alertMessage ?></h2>
<div class="row">
    <div class="col-3"></div>
    <form action="/default/doTransfer" method="post" class="col-6">
        <div class="form-group">
            <label for="email">Email of the user you want to transfer to</label>
            <input id="email" name="email" type="email" class="form-control">
        </div>
        <div class="form-group">
            <label for="amount">Amount</label>
            <input id="amount" name="amount" type="number" class="form-control">
        </div>
        <button type="submit" name="send" class="btn btn-primary">Submit</button>
    </form>
    <div class="col-3"></div>
</div>
