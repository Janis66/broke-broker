<div id="container" class="container">
    <div style="text-align:center">
        <h2>Comments</h2>
    </div>
    <div class="row">
        <div class="column">
            <img src="/images/title_picture.jpg" style="width:100%">
        </div>
        <div class="column">
            <?php if ($_SESSION["edit"] = false) { ?>
                <form action="/default/doComment" method="post">
                    <label for="name">First Name</label>
                    <input type="text" id="name" name="name" placeholder="Your name..">
                    <label for="text">Submit your own Comment</label>
                    <textarea id="text" name="text" placeholder="Comment something.." style="height:170px"></textarea>
                    <input type="submit" value="Submit">
                </form>
            <?php } else { ?>
                <form action="/default/doComment" method="post">
                    <label for="name">First Name</label>
                    <input type="text" id="name" name="name" placeholder="Your name.." value="<?= $commentName?>">
                    <label for="text">Submit your own Comment</label>
                    <textarea id="text" name="text" placeholder="Comment something.." style="height:170px"><?= $commentText?></textarea>
                    <input type="submit" value="Submit">
                </form>
            <?php } ?>
        </div>
    </div>
    <?php if (empty($comments)): ?>
        <div class="dhd">
            <h2 class="">No Comments yet.</h2>
        </div>
    <?php else: ?>
        <?php foreach ($comments as $comment): ?>
            <div class="panel panel-default">
                <div class="panel-heading"><?= $comment->name; ?></div>
                <div class="panel-body">
                    <p class="description"><?= $comment->text; ?></p>
                    <p>
                        <a title="Delete" href="/default/delete?id=<?= $comment->id; ?>">Delete</a>
                    </p>
                    <p>
                        <a title="Edit" href="/default/edit?id=<?= $comment->id; ?>">Edit</a>
                    </p>
                </div>
            </div>
        <?php endforeach;
        endif;?>
</div>