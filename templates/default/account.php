<?php

use App\Repository\UserRepository;

?>
<div>
    <h1 class="account-title">Welcome</h1>
    <h1 class="account-title"><?=
        $firstName ?></h1>
</div>

<div class="container">
    <div class="row">
        <div class="col-6">
            <h1 style="float: left;" id="balance">Total Balance: <?php
                $userRepository = new UserRepository();
                echo $userRepository->getBalance($_SESSION["usedEmail"]); ?>.00$</h1>
        </div>
        <div class="col-6">
            <h1 id="accountName" style="float: left;">Full name: <?= $firstName ?> <?= $lastName ?></h1>
        </div>
    </div>
    <div class="row">
        <div class="col-6">

        </div>
        <div class="col-6">

            <h1 id="changeText" style="float: left;">Change password:</h1>

            <form id="change" action="/user/changePassword">
                <button type="submit" style="float: left; margin: 10px;">change</button>
            </form>

        </div>
    </div>
    <div class="row">
        <div class="col-6">
        </div>
        <div class="col-6">
            <h1 id="changeText" style="float: left;">Creation Date: <?= date("d. F Y", strtotime($timeStamp)) ?></h1>
        </div>
    </div>
    <div class="row">
        <div class="col-6">
        </div>
        <div class="col-6">
            <a href="/user/logout" style="float: left;" id="log-out">Log out</a>
        </div>
    </div>
</div>
</div>

