<div class="title">
<h1 id="titletext">Broke Broker</h1>
<h2 id="slogan">Think Broke.</h2>
</div>

<div class="grid-container">
    <div class=grid-text>This is a banking website with virtual credits,
        where you can invest in stocks and sell them to get a profit.
        Each user will start with 1000 credits and
        every day the user can collect 100 free credits by clicking the “Collect Daily Reward” button in the account tab.
        Those Credits will be transferred straight to the user account balance, which you can later spend on Stocks or transfer it to a friend.
    </div>
    <div class=grid-title>General</div>
    <div class=grid-title>Stocks</div>
    <div class=grid-text>In the stocks tab, you can invest your money in multiple different stocks. There are 12
        different stocks which get updated daily. It’s recommended to check every day if the value of the stock you’ve
        invested in, has increased, or decreased. To make the highest possible profit, you should invest when you think
        the value of a stock is going to increase but is low right now. You can then sell the stocks if the value has
        increased.
    </div>
    <div class=grid-text>Here you will find the best/luckiest stock investors on our Website.
        On the leaderboard you will be able to see the users name and current balance.
        The top 3 will be displayed on a podium.
        Below the podium you can see a list of the top 10 users.
        There will be also a separate leaderboard with a podium and a top 10 list for the people with the all-time highest
        balance.
    </div>
    <div class=grid-title>Leaderboards</div>
    <div class=grid-title>Transfer</div>
    <div class=grid-text>Transferring credits and stocks is one of our latest/newest features on our Website.
        Here you can Transfer credits or stocks to your friends or family members.
        With Transferring you could combine all of your users credits and get one of your accounts to the top of the
        leaderboard.
    </div>
</div>