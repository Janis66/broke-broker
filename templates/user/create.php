<h2 id="alertMessage" ><?= $alertMessage ?></h2>
<div class="row">
    <div class="col-3"></div>
    <form action="/user/doCreate" method="post" class="col-6">
        <div class="form-group">
            <label for="fname">First name</label>
            <input id="fname" name="fname" type="text" class="form-control">
        </div>
        <div class="form-group">
            <label for="lname">Last name</label>
            <input id="lname" name="lname" type="text" class="form-control">
        </div>
        <div class="form-group">
            <label for="email">Mail</label>
            <input id="email" name="email" type="text" class="form-control">
        </div>
        <div class="form-group">
            <label class="control-label" for="password">Password</label>
            <input id="password" name="password" type="password" class="form-control">
        </div>
        <button type="submit" name="send" class="btn btn-primary">Submit</button>
    </form>
    <div class="col-3"></div>
</div>
