<div class="row">
<form action="/user/doChangePassword" method="post" class="col-6">
    <div class="form-group">
        <label class="control-label" for="password">Current Password</label>
        <input id="password-old" name="password-old" type="password" class="form-control">
    </div>
    <div class="form-group">
        <label class="control-label" for="password">New Password</label>
        <input id="password" name="password" type="password" class="form-control">
    </div>
    <div class="form-group">
        <label class="control-label" for="password">Repeat your Password</label>
        <input id="password-repeat" name="password-repeat" type="password" class="form-control">
    </div>
    <button type="submit" name="send" class="btn btn-primary">Submit</button>
</form>
</div>