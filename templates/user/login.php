<h2 id="alertMessage" ><?= $alertMessage ?></h2>
<div id="login" class="row">
    <div class="col-3">

    </div>
    <div class="col-6">
	<form action="/user/doLogin" method="post" >
		<div class="form-group">
            <label for="email">Mail</label>
            <input id="email" name="email" type="email" class="form-control">
        </div>
        <div class="form-group">
            <label for="password">Password</label>
            <input id="password" name="password" type="password" class="form-control">
        </div>
		<button type="submit" name="send" class="btn btn-primary">Submit</button>
	</form>
    <a class="registerLink">Don't have an account? -> </a><a class="registerLink" href="/user/create">register here</a>
    </div>
    <div class="col-3">
    </div>
</div>
