<?php
use App\Repository\UserRepository;
?>
<!doctype html>
<html lang="de">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="/css/style.css">

    <title><?= $title; ?> | Broke Broker</title>
</head>
<body>
<div class="grid">
    <a href="/" id="logoNav"></a>
    <div class="navbar">
        <a href="/default/stocks" id="stocksLink">Stocks</a>
        <a href="/default/transfer" id="transferLink">Transfer</a>
        <a href="/default/leaderboard" id="leaderboardLink">Leaderboard</a>
        <?php if (($_SESSION["loggedIn"]) == false) {?>
            <a href="/user/login" id="loginButton">Login</a>
            <a id="balanceText" >0.00 $</a>
        <?php } else {?>
            <a href="/default/account" id="accountLink">Account</a>
            <a id="balanceText" ><?php
                $userRepository = new UserRepository();
                echo $userRepository->getBalance($_SESSION["usedEmail"]);
            ?>.00 $</a>
        <?php }?>
    </div>
</div>

<main class="container">
    <h1 id="heading"><?= $heading; ?></h1>
</main>
</body>
</html>